import React from 'react';
import ReactDOM from 'react-dom';
import CommentDetail from './CommentDetail';
import ApprovalCard from './ApprovalCard';
import Faker from 'faker';

const App = () =>{
    return (
        <div className="ui container comments">
            <ApprovalCard>
                <CommentDetail author="Bintang" time="Today at 6 PM" avatar={Faker.image.avatar()} comment="Halo Gan!"/>
            </ApprovalCard>
            <ApprovalCard>
                <CommentDetail author="Aldi" time="Today at 3 PM"  avatar={Faker.image.avatar()} comment="Asli Gan!"/>
            </ApprovalCard>            
            <ApprovalCard>            
                <CommentDetail author="Diva" time="Today at 8 PM" avatar={Faker.image.avatar()} comment="Palsu Gan!"/>
            </ApprovalCard>                    
        </div>
    );
};

ReactDOM.render(<App />, document.querySelector('#root'));